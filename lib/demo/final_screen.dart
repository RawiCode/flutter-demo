import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MoodTrackerScreen());

class MoodTrackerScreen extends StatefulWidget {
  @override
  _MoodTrackerScreenState createState() => _MoodTrackerScreenState();
}

class _MoodTrackerScreenState extends State<MoodTrackerScreen> {
  int likeCount = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Demo'),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.favorite),
          onPressed: () {
            setState(() {
              likeCount++;
            });
            print(likeCount);
          },
        ),
        body: SafeArea(
          child: Center(
            child: Text(
              'Flutter +$likeCount',
              style: TextStyle(
                  fontSize: 30, fontFamily: 'Pacifico', color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
