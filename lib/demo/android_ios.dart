import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../const.dart';
import '../mood.dart';

void main() => runApp(MoodTrackerScreen());

class MoodTrackerScreen extends StatefulWidget {
  @override
  _MoodTrackerScreenState createState() => _MoodTrackerScreenState();
}

class _MoodTrackerScreenState extends State<MoodTrackerScreen> {
  Mood selectedMood = moodList[0];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        backgroundColor: Colors.teal,
        body: SafeArea(
          child: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Flutter Demo',
                  style: TextStyle(
                    fontSize: 40.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Pacifico',
                  ),
                ),
                SizedBox(
                  height: 20.0,
                  width: 150.0,
                  child: Divider(
                    color: Colors.teal.shade100,
                  ),
                ),
                Text(
                  'KAT Knowledge'.toUpperCase(),
                  style: TextStyle(
                    fontFamily: 'SourceSansPro',
                    fontSize: 18.0,
                    color: Colors.teal.shade100,
                    letterSpacing: 2.5,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 30),
                  child: Icon(
                    selectedMood.iconData,
                    color: Colors.teal.shade200,
                    size: 200,
                  ),
                ),
                Platform.isIOS ? iOSPicker() : androidDropdown()
              ],
            ),
          ),
        ),
      ),
    );
  }

  DropdownButton<Mood> androidDropdown() {
    List<DropdownMenuItem<Mood>> dropdownItems = [];
    for (Mood mood in moodList) {
      var newItem = DropdownMenuItem(
        child: Text(
          mood.name,
          style: TextStyle(
            color: Colors.teal.shade200,
          ),
        ),
        value: mood,
      );
      dropdownItems.add(newItem);
    }

    return DropdownButton<Mood>(
      value: selectedMood,
      items: dropdownItems,
      icon: Icon(
        Icons.arrow_drop_down,
        color: Colors.white,
      ),
      onChanged: (value) {
        setState(() {
          selectedMood = value;
        });
      },
    );
  }

  Container iOSPicker() {
    List<Text> pickerItems = [];
    for (Mood mood in moodList) {
      pickerItems.add(Text(
        mood.name,
        style: TextStyle(
          color: Colors.teal.shade200,
        ),
      ));
    }

    return Container(
      height: 100,
      width: double.infinity,
      child: CupertinoPicker(
        itemExtent: 32.0,
        backgroundColor: Colors.transparent,
        onSelectedItemChanged: (selectedIndex) {
          setState(() {
            selectedMood = moodList[selectedIndex];
          });
        },
        children: pickerItems,
      ),
    );
  }
}
