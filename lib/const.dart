import 'package:flutter/material.dart';
import 'package:flutter_demo/mood.dart';

const List<Mood> moodList = [
  Mood(name: 'Very sad', iconData: Icons.sentiment_very_dissatisfied),
  Mood(name: 'Sad', iconData: Icons.sentiment_dissatisfied),
  Mood(name: 'Normal', iconData: Icons.sentiment_neutral),
  Mood(name: 'Happy', iconData: Icons.sentiment_satisfied),
  Mood(name: 'Very Happy', iconData: Icons.sentiment_very_satisfied),
];
