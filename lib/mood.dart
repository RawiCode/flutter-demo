import 'package:flutter/cupertino.dart';

class Mood {
  final String name;
  final IconData iconData;

  const Mood({this.name, this.iconData});
}
